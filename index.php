<!doctype html>
<html>

<head>
    <?php require('inc_header.php'); ?>
        <link rel="stylesheet" href="owlcarousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="owlcarousel/assets/owl.theme.default.min.css">
        <script src="owlcarousel/owl.carousel.min.js"></script>
        <link rel="stylesheet" href="flexslider/flexslider.css" type="text/css" media="screen" />
        <!-- Modernizr -->
        <script src="flexslider/demo/js/modernizr.js"></script>
        <script defer src="flexslider/jquery.flexslider.js"></script>
        <script type="text/javascript" src="flexslider/demo/js/shCore.js"></script>
        <script type="text/javascript" src="flexslider/demo/js/shBrushJScript.js"></script>
        <style>
			.owl-theme .owl-nav.disabled+.owl-dots{
				margin-top: -30px;
				/* z-index: 99999999; */
				position:absolute;
				left: 35%;
			}
            .owl-media .owl-carousel .owl-item img {
                width: auto !important;
            }
            
			.owl-newstop.owl-theme .owl-nav [class*=owl-]{
				position: absolute;
                top: 40%;
                background-color: transparent;
                color: #888888;
                font-size: 45px;
                outline: 0;
                margin: 0;
			}
            .owl-news.owl-theme .owl-nav [class*=owl-] {
                position: absolute;
                top: 10%;
                background-color: transparent;
                color: #888888;
                font-size: 45px;
                outline: 0;
                margin: 0;
            }
			 .owl-banner.owl-theme .owl-nav [class*=owl-] {
                background-color: transparent;
                color: #888888;
                font-size: 45px;
                outline: 0;
                margin: 0;
				 position: absolute;
				 z-index: 9;
            }
            
			.owl-banner.owl-theme .owl-nav [class*=owl-] span,
            .owl-newstop.owl-theme .owl-nav [class*=owl-] span,
            .owl-news.owl-theme .owl-nav [class*=owl-] span {
                padding: 0 10px;
                opacity: 0;
            }
			
			 .owl-banner.owl-theme .owl-nav .owl-prev {
                right: 200px;
                background-image: url(images/arrow_left.png);
                background-repeat: no-repeat;
                background-position: center;
				 bottom: 0;
            }
            
            
            .owl-newstop.owl-theme .owl-nav .owl-prev {
                left: -35px;
                background-image: url(images/prevnac.jpg);
                background-repeat: no-repeat;
                background-position: center;
            }
            
            .owl-news.owl-theme .owl-nav .owl-prev {
                left: -40px;
                background-image: url(images/prevgrey.png);
                background-repeat: no-repeat;
                background-position: center;
            }
            
			  .owl-banner.owl-theme .owl-nav .owl-next {
			 	right: 145px;
                background-image: url(images/arrow_right.png);
                background-repeat: no-repeat;
                background-position: center;
				  bottom: 0;
            }
            .owl-newstop.owl-theme .owl-nav .owl-next {
			 	right: -30px;
                background-image: url(images/nextnac.jpg);
                background-repeat: no-repeat;
                background-position: center;
            }
            
            .owl-news.owl-theme .owl-nav .owl-next {
                right: -40px;
                background-image: url(images/nextgrey.png);
                background-repeat: no-repeat;
                background-position: center;
            }
            
            .owl-newsmobile .owl-theme .owl-nav [class*=owl-] {
                position: absolute;
                top: 30%;
                background-color: transparent;
                color: #888888;
                font-size: 45px;
                outline: 0;
                margin: 0;
            }
            
            .owl-newsmobile.owl-theme .owl-nav [class*=owl-] span {
                padding: 0 10px;
                opacity: 0;
            }
            
            .owl-newsmobile.owl-theme .owl-nav .owl-prev {
                left: -40px;
                background-image: url(images/prevgrey.png);
                background-repeat: no-repeat;
                background-position: center;
            }
            
            .owl-newsmobile.owl-theme .owl-nav .owl-next {
                right: -40px;
                background-image: url(images/nextgrey.png);
                background-repeat: no-repeat;
                background-position: center;
            }
            
            .owl-link.owl-theme .owl-nav [class*=owl-] {
                position: absolute;
                top: 15%;
                background-color: transparent;
                color: #888888;
                font-size: 45px;
                outline: 0;
                margin: 0;
            }
            
            .owl-link.owl-theme .owl-nav [class*=owl-] span {
                padding: 0 10px;
                opacity: 0;
            }
            
            .owl-link.owl-theme .owl-nav .owl-prev {
                left: -40px;
                background-image: url(images/prevgrey.png);
                background-repeat: no-repeat;
                background-position: center;
            }
            
            .owl-link.owl-theme .owl-nav .owl-next {
                right: -40px;
                background-image: url(images/nextgrey.png);
                background-repeat: no-repeat;
                background-position: center;
            }
            .owl-theme .owl-dots .owl-dot span{
                margin: 5px;
            }
			@media (max-width: 1600px) {
				.owl-theme .owl-nav.disabled+.owl-dots{
					left: 40%;
				}
			}
			@media (max-width: 1440px) {
				.owl-theme .owl-nav.disabled+.owl-dots{
					left: 45%;
				}
			}
            @media (max-width: 1250px) {
                .owl-news.owl-theme .owl-nav .owl-next {
                    right: 0;
                }
                .owl-news.owl-theme .owl-nav .owl-prev {
                    left: 0;
                }
                .owl-newsmobile.owl-theme .owl-nav .owl-next {
                    right: 0;
                }
                .owl-newsmobile.owl-theme .owl-nav .owl-prev {
                    left: 0;
                }
                .owl-link.owl-theme .owl-nav .owl-next {
                    right: 0;
                }
                .owl-link.owl-theme .owl-nav .owl-prev {
                    left: 0;
                }
				.owl-theme .owl-nav.disabled+.owl-dots{
					left: 45%;
				}
            }
            
           
            
            @media (max-width: 991px) {

                .owl-news.owl-theme .owl-nav .owl-next {
                    right: -15px;
                }
                .owl-news.owl-theme .owl-nav .owl-prev {
                    left: -15px;
                }
                .owl-news.owl-theme .owl-nav [class*=owl-] span {
                    padding: 0;
                }
								.owl-theme .owl-nav.disabled+.owl-dots{
					left: 45%;
			}
            }
            
			.owl-banner .owl-stage{
				padding-left: 0px !important;
			}
            @media (max-width: 767px) {
             
                .owl-news.owl-theme .owl-nav .owl-next {
                    right: -15px;
                }
                .owl-news.owl-theme .owl-nav .owl-prev {
                    left: -15px;
                }
				.owl-banner .owl-stage{
				padding-left:15px;
			}
				.owl-theme .owl-nav.disabled+.owl-dots{
				/* z-index: 99999999; */
				position:relative;
					padding-bottom: 30px;
					left: 0%;
			}
              
              
            }
			
        </style>
</head>

<body>
    <div class="container-fluid">
        <?php require('inc_menu.php'); ?>
        <div class="banner-crop">
         <div class="row">
         
         	<div class="col-sm-4 hidden-xs">
				 <div class="banner-txt-slide">
          			<strong id="banner-data1">NationTTal - 1</strong> 
          			<span id="banner-data2">Innovation Agency</span> <br>
          			<span id="banner-data3">Lorem Ipsum is simply dummy text of the printing and typesetting industry 
There are many variations of passages</span>
          		</div>
          		<a href="#" class="regisbtn">ลงทะเบียน</a>
          		<!--<div class="social-fixed">
         				<a href="#"><img src="images/fb-icon.png"></a>
         				<a href="#"><img src="images/ig-icon.png"></a>
         				<a href="#"><img src="images/youtube-icon.png"></a>
         			</div>-->
         	</div>
         	<div class="col-xs-12 col-sm-8" style="padding-right:0px;">
        		         	<!--<div class="bgwhite"></div>-->

         	<div class="owl-banner owl-carousel owl-theme mb-5">
				   <div>
					<img src="images/banner_index.png" data1="NationTTal - 2" data2="Innovation Agency" data3="Lorem Ipsum is simply dummy text of the printing and typesetting industry 
		There are many variations of passages">
				   </div>
					<div>
					 <img src="images/banner_index2.png" data1="NationTTal - 3" data2="Innovation Agency" data3="Lorem Ipsum is simply dummy text of the printing and typesetting industry 
		There are many variations of passages">
				   </div>
		   </div>
         	</div>
         	<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
				 <div class="banner-txt-slide">
          			<strong id="banner-data1">NationTTal - 55555555555</strong> 
          			<span id="banner-data2">Innovation Agency</span> <br>
          			<span id="banner-data3">Lorem Ipsum is simply dummy text of the printing and typesetting industry 
There are many variations of passages</span>
          		</div>
          		<a href="#" class="regisbtn">ลงทะเบียน</a>
          		<!--<div class="social-fixed">
         				<a href="#"><img src="images/fb-icon.png"></a>
         				<a href="#"><img src="images/ig-icon.png"></a>
         				<a href="#"><img src="images/youtube-icon.png"></a>
         			</div>-->
         	</div>
         	
         </div>
         </div>
            <div class="container">
                <section class="row wow fadeInDown">
                    <div class="col-xs-12 col-sm-9">
                        <div class="owl-newstop owl-carousel owl-theme">
                            <div>
                                <div class="product_img two"> <img src="images/news.png"> <span class="caption">Lorem Ipsum is simply dummy   </span> </div>
                            </div>
                            <div>
                                <div class="product_img two"> <img src="images/news1.png"> <span class="caption">Lorem Ipsum is simply dummy </span> </div>
                            </div>
                            <div>
                                <div class="product_img two"> <img src="images/news2.png"> <span class="caption">Lorem Ipsum is simply dummy </span> </div>
                            </div>
                            <div>
                                <div class="product_img two"> <img src="images/news3.png"> <span class="caption">Lorem Ipsum is simply dummy </span> </div>
                            </div>
                            <div>
                                <div class="product_img two"> <img src="images/news1.png"> <span class="caption">Lorem Ipsum is simply dummy </span> </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="event-news-bg">
                            <h1><span class="boldtxt">Event</span><br> News</h1>
                            <h3>ข่าวสารกิจกรรม</h3>
                            <a class="read-more-event" href="#"><img src="images/plus_19.png"></a>
                        </div>
                    </div>
                </section>
                <section class="row wow fadeInDown wrap_media">
                    <div class="col-xs-12 col-sm-4 home_head inmobile2">
                        <h1><span class="boldtxt">Media</span></h1>
                        <h3>สื่อนวัตกรรม</h3>
                        <p class="txt-media">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry' standard dummy </p>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <div class="row">
                            <div class="owl-media owl-carousel owl-theme">
                                <div>
                                    <div class="media_col">
                                        <a href="#" class="media_logo">
                                            <div><img src="images/media-1.png"><img src="images//media-1-hover.png"></div> วิดีทัศน์ </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="media_col">
                                        <a href="#" class="media_logo">
                                            <div><img src="images/media-2.png"><img src="images/media-2-hover.png"></div> หนังสือ </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="media_col">
                                        <a href="#" class="media_logo">
                                            <div><img src="images/media3.png"><img src="images/media3-hover.png"></div> อินโฟกราฟฟิก </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="media_col">
                                        <a href="#" class="media_logo">
                                            <div><img src="images/media4.png"><img src="images/media4-hover.png"></div> บล็อก </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="media_col">
                                        <a href="#" class="media_logo">
                                            <div><img src="images/media-1.png"><img src="images/media-1-hover.png"></div> วิดีทัศน์ </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
                <!--<div class="container wrap_innomec">
                    <section class="row wow fadeInDown mt-5 mb-5">
                        <div class="row innovationcl">
                           <span class="bgdarkblue"></span>
                            <div class="col-sm-4 hidden-sm hidden-md hidden-lg">
                                <div class="innova-head">
                                    <h1><span class="boldtxt">Innovation 
                                    </span> Mechanism</h1>
                                    <h3>ขอรับการสนับสนุนโครงการนวัตกรรม</h3> </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="">
                                            <div class="circle">
                                                <div class="circle__inner">
                                                    <div class="circle__wrapper">
                                                        <div class="circle__content"> นวัตกรรมแบบเปิด <img src="images/logo/logo-innovation-2.png"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="">
                                            <div class="circle">
                                                <div class="circle__inner">
                                                    <div class="circle__wrapper">
                                                        <div class="circle__content"> นวัตกรรมเพื่อสังคม <img src="images/logo/logo-socailinno-2.png"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="">
                                            <div class="circle">
                                                <div class="circle__inner">
                                                    <div class="circle__wrapper">
                                                        <div class="circle__content"> <span class="boldtxt">MIND CREDIT</span> <img src="images/logo/logo-mine-2.png"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="">
                                            <div class="circle">
                                                <div class="circle__inner">
                                                    <div class="circle__wrapper">
                                                        <div class="circle__content"> นวัตกรรมมุ่งเป้า <img src="images/logo/logo-thematic-2.png"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 hidden-xs">
                                <div class="innova-head">
                                    <h1><span class="boldtxt">Innovation 
                                    </span> Mechanism</h1>
                                    <h3>ขอรับการสนับสนุนโครงการนวัตกรรม</h3> </div>
                            </div>
                        </div>
                    </section>
                </div>-->
            <section class="row wow fadeInDown">
                <div class="col-xs-12 bg_innomech">
                    <span></span>
                    <div class="row">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 innomech_col">
                                    <a href="#" class="innomech_logo">
                                        นวัตกรรมเศรษฐกิจ
                                        <div><img src="images/logo/innoeco_w.png"><img src="images/logo/innoeco_g.png"></div>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-4 innomech_col">
                                    <a href="#" class="innomech_logo">
                                        นวัตกรรมเพื่อสังคม
                                        <div><img src="images/logo/logo-socailinno-2.png"><img src="images/logo/logo-socailinno-1.png"></div>
                                    </a>
                                </div>
                                <hgroup class="col-xs-12 col-sm-4 innomech">
                                    <h1>Innovation</h1> 
                                    <h2>Mechanism</h2>
                                    <h3>ขอรับการสนับสนุนโครงการนวัตกรรม</h3>
                                </hgroup>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="row wow fadeInDown  mt-20">
                <div class="col-xs-12 bg_news"> <span></span>
                    <div class="row">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 home_head_awards inmobile">
                                    <h1><span class="boldtxt">INNOVATION</span> AWARDS</h1>
                                    <h3>รางวัลนวัตกรรม</h3> <a href="#">ดูทั้งหมด</a> </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="owl-news owl-carousel owl-theme">
                                        <div>
                                            <figure class="home_news_item">
                                                <a href="#"><img src="images/innovation_39.png"></a>
                                                <figcaption>
                                                    <h4 class="dotmaster">NIA ร่วมกับ บิสเนส ฟรานซ์ (ฝ่ายการค้าและพาณิชย์) สถานเอกอัครราชทูตฝรั่งเศสประจำประเทศไทย</h4>
                                                    <p class="dotmaster">จัดงาน THAI-FRENCH SMART CITY FORUM เพื่อเป็นเวทีแลกเปลี่ยนความรู้และประสบการณ์ด้านการพัฒนาเมืองอัจฉริยะ ระหว่างสองประเทศ รวมถึงการพัฒนาระบบนิเวศ</p>
                                                </figcaption> <a href="#" class="btnmore">อ่านต่อ</a> </figure>
                                        </div>
                                        <div>
                                            <figure class="home_news_item">
                                                <a href="#"><img src="images/innovation_41.png"></a>
                                                <figcaption>
                                                    <h4 class="dotmaster">ประกวดภาพถ่ายย่านนวัตกรรม “Innovation Thailand Photo Contest 2018”</h4>
                                                    <p class="dotmaster">ผลงานผู้ชนะโครงการประกวดภาพถ่ายย่านนวัตกรรม “Innovation Thailand Photo Contest 2018” ในธีม One Shot Knock Out : Innovation District แมตช์แรกที่ภูเก็ต</p>
                                                </figcaption> <a href="#" class="btnmore">อ่านต่อ</a> </figure>
                                        </div>
                                        <div>
                                            <figure class="home_news_item">
                                                <a href="#"><img src="images/innovation_43.png"></a>
                                                <figcaption>
                                                    <h4 class="dotmaster">พิธีเปิดนิทรรศการ “ร้อยคนไทยหัวใจนวัตกรรม” (100 Faces of Thailand’s Innovation Inspirers)</h4>
                                                    <p class="dotmaster">NIA จัดพิธีเปิดนิทรรศการ “ร้อยคนไทยหัวใจนวัตกรรม” (100 Faces of Thailand’s Innovation Inspirers) โดยได้รับเกียรติจาก นายสนิท ศรีวิหค รองผู้ว่าราชการจังหวัดภูเก็ต</p>
                                                </figcaption> <a href="#" class="btnmore">อ่านต่อ</a> </figure>
                                        </div>
                                        <div>
                                            <figure class="home_news_item">
                                                <a href="#"><img src="images/innovation_41.png"></a>
                                                <figcaption>
                                                    <h4 class="dotmaster">ประกวดภาพถ่ายย่านนวัตกรรม “Innovation Thailand Photo Contest 2018”</h4>
                                                    <p class="dotmaster">ผลงานผู้ชนะโครงการประกวดภาพถ่ายย่านนวัตกรรม “Innovation Thailand Photo Contest 2018” ในธีม One Shot Knock Out : Innovation District แมตช์แรกที่ภูเก็ต</p>
                                                </figcaption> <a href="#" class="btnmore">อ่านต่อ</a> </figure>
                                        </div>
                                        <div>
                                            <figure class="home_news_item">
                                                <a href="#"><img src="images/innovation_41.png"></a>
                                                <figcaption>
                                                    <h4 class="dotmaster">ประกวดภาพถ่ายย่านนวัตกรรม “Innovation Thailand Photo Contest 2018”</h4>
                                                    <p class="dotmaster">ผลงานผู้ชนะโครงการประกวดภาพถ่ายย่านนวัตกรรม “Innovation Thailand Photo Contest 2018” ในธีม One Shot Knock Out : Innovation District แมตช์แรกที่ภูเก็ต</p>
                                                </figcaption> <a href="#" class="btnmore">อ่านต่อ</a> </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            		<section class="row wow fadeInDown wrap_interestinno mt-5">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="innova-head-int">
								<h1><span class="boldtxt">Interesting 
				</span> innovation</h1>
								<h3 class="righttxt">นวัตกรรมที่น่าสนใจ</h3> </div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-6 col-sm-6">
						<a href="#">
						<div class="border-hover">
							<div class="row">
								<div class="col-sm-5"> <img src="images/inno-new_03.png" class="img-responsive"> </div>
								<div class="col-sm-7 padleft">
									<figure class="home_int-inno_item">
										<figcaption>
											<h4 class="dotmaster">Lorem Ipsum is simply dummy text</h4>
											<p class="dotmaster">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
											
											<div class="share-calendar">
												<li><a href="#"><i class="far fa-calendar-alt"></i> 28 August 2016</a></li>
												 <li><a href="#"><i class="fas fa-share-alt"></i> Share</a> </li>
											</div>
											
										</figcaption>
									</figure>
								</div>
								</div>
							</div>
							</a>
						</div>
						<div class="col-xs-6 col-sm-6">
						<a href="#">
						<div class="border-hover">
							<div class="row">
								<div class="col-sm-5"> <img src="images/inno-new_05.png" class="img-responsive"> </div>
								<div class="col-sm-7 padleft">
									<figure class="home_int-inno_item">
										<figcaption>
											<h4 class="dotmaster">Lorem Ipsum is simply dummy text</h4>
											<p class="dotmaster">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
											
											<div class="share-calendar">
												<li><a href="#"><i class="far fa-calendar-alt"></i> 28 August 2016</a></li>
												 <li><a href="#"><i class="fas fa-share-alt"></i> Share</a> </li>
											</div>
											
										</figcaption>
									</figure>
								</div>
								</div>
							</div>
							</a>
						</div>
					</div>
				</div>
			</section>
			<br><br>
		
            
            <section class="row wow fadeInDown">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 head_link">
                            <h1>ลิงค์กิจกรรม</h1> </div>
                    </div>
                    <div class="row margin_activities_link">
                        <div class="col-xs-12">
                            <div class="owl-link owl-carousel owl-theme">
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link01.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link02.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link03.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link04.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link01.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link02.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link03.jpg"></a>
                                    </figure>
                                </div>
                                <div>
                                    <figure>
                                        <a href="#"><img src="images/activitie_link04.jpg"></a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php require('inc_footer.php'); ?>
    </div>
    <script>
        $(window).on("load resize", function () {
            var offsetctn = $('.container').offset();
            var home_news_item = $('.home_news_item').width();
            var home_head = $('.home_head').offset();
            var container_width = $('.container').width();
            console.log(offsetctn);
            $(' .bg_news > span').css('width', offsetctn.left + (container_width / 2) - 30);
            var blogh = $('.padding_blog .home_head').outerHeight() + $('.blog_item img').outerHeight() + 80;
            $('.bg_blog > span').css('height', blogh);
        });
    </script>
    <script type="text/javascript">
        $(function () {
            SyntaxHighlighter.all();
        });
        $(window).on("load", function () {
            $('.flexslider').flexslider({
                animation: "slide"
                , controlNav: false
                , start: function (slider) {
                    $(".dotmaster").trigger("update.dot");
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.owl-news').on('initialized.owl.carousel', function (event) {
                $(".dotmaster").trigger("update.dot");
            });
            $(".owl-news").owlCarousel({
                loop: false
                , margin: 20
                , nav: true
                , dots: false
                , autoplay: false
                , autoplayTimeout: 6000
                , slideBy: 1
                , responsive: {
                    0: {
                        items: 1
                        , margin: 15, //slideBy: 3
                    }
                    , 500: {
                        items: 2
                    }
                    , 768: {
                        margin: 30
                        , items: 3
                    }
                    , 992: {
                        margin: 80
                        , items: 3
                    }
                    , 1025: {
                        margin: 80
                        , items: 3
                    }
                }
            });
            $(".owl-newstop").owlCarousel({
                loop: true
                , margin: 20
                , nav: true
				, smartSpeed: 1000
                , dots: false
                , autoplay: true
                , autoplayTimeout: 6000
                , slideBy: 1
                , responsive: {
                    0: {
                        items: 1
                        , margin: 15, //slideBy: 3
                    }
                    , 500: {
                        items: 2
                    }
                    , 768: {
                        margin: 20
                        , items: 4
                    }
                    , 992: {
                        margin: 10
                        , items: 4
                    }
                    , 1025: {
                        margin: 10
                        , items: 4
                    }
                }
            });
            $('.owl-newsmobile').on('initialized.owl.carousel', function (event) {
                $(".dotmaster").trigger("update.dot");
            });
            $(".owl-newsmobile").owlCarousel({
                loop: false
                , margin: 20
                , nav: true
                , dots: false
                , autoplay: true
                , autoplayTimeout: 6000
                , slideBy: 1
                , items: 1
            });
            $(".owl-link").owlCarousel({
                loop: false
                , margin: 20
                , nav: false
                , dots: false
                , autoplay: false
                , autoplayTimeout: 6000
                , slideBy: 1
                , responsive: {
                    0: {
                        items: 1
                        , margin: 15
                        , slideBy: 1
                    }
                    , 500: {
                        items: 2
                        , slideBy: 2
                    }
                    , 768: {
                        margin: 30
                        , items: 3
                        , slideBy: 3
                    }
                    , 992: {
                        margin: 30
                        , items: 4
                        , slideBy: 4
                    }
                    , 1025: {
                        margin: 35
                        , items: 4
                        , slideBy: 4
                    }
                }
            });
            $(".owl-media").owlCarousel({
                loop: true
                , margin: 7
                , nav: false
                , dots: false
                , autoplay: true
                , autoplayTimeout: 6000
				, smartSpeed: 1000
                , slideBy: 1
                , responsive: {
                    0: {
                        items: 2
                        , margin: 7
                        , slideBy: 2
                    }
                    , 500: {
                        items: 2
                        , slideBy: 2
                    }
                    , 768: {
                        margin: 7
                        , items: 3
                        , slideBy: 3
                    }
                    , 992: {
                        margin: 7
                        , items: 4
                        , slideBy: 4
                    }
                    , 1025: {
                        margin: 7
                        , items: 4
                        , slideBy: 4
                    }
                }
            });
			
			var owlstagepad = $('.container').offset().left/2;
			$('.banner-txt-slide').css('left', owlstagepad*2 - 1);
			$('.social-fixed').css('left', owlstagepad*2 - 1);
			$('.regisbtn').css('left', owlstagepad*2 - 1);
			$('.customowlnext').css('right', owlstagepad);
			
			//$('.owl-banner').on('initialized.owl.carousel', function (event) {
            //    $('.customowlnext').css('right', owlstagepad);
            //});
			
			var owlright = $('.container').offset().left;
			var owlleft = $('.container').offset().left+60;
			
			$('.owl-banner').owlCarousel({
				stagePadding: owlstagepad,
				loop:true,
				margin:50,
				smartSpeed: 1000,
				autoplay: true,
                autoplayTimeout: 6000,
//				nav:true,
//				navText: ["<div style='right:"+owlleft+"px' class='customowlprev'></div>","<div style='right:"+owlright+"px' class='customowlnext'></div>"],
				dots: true,
				responsive:{
					0:{
						items:1
					},
					600:{
						items:1
					},
					1000:{
						
						items:1
						
					}
				}
			});
			$('.owl-banner').on('changed.owl.carousel',function(event){
				$(".banner-txt-slide").hide();
			});
			$('.owl-banner').on('changed.owl.carousel',function(event){
				console.log(event);
				
				setTimeout(function(){
					var data1 = $(event.currentTarget.innerHTML).find(".active").find("img").attr("data1");
					var data2 = $(event.currentTarget.innerHTML).find(".active").find("img").attr("data2");
					var data3 = $(event.currentTarget.innerHTML).find(".active").find("img").attr("data3");
					$("#banner-data1").text(data1);
					$("#banner-data2").text(data2);
					$("#banner-data3").text(data3);
					$(".banner-txt-slide").fadeIn(500);
				},100);
				
			});
			
        });
    </script>
    
    <style>
		.owl-banner.owl-theme .owl-nav [class*=owl-]{
			position: static;
		}
		.customowlnext{
				position: absolute;
			bottom: 38px;
			background-color: #FFF;
			width: 80px;
		    background-image: url(images/arrow_right1.png);
  			background-repeat: no-repeat;
            background-position: center;
			height: 40px;
		}
		.customowlnext:hover{
			background-image: url(images/arrow_right.png);
			-webkit-transition: all 0.5s ease-in-out;
			-moz-transition: all 0.5s ease-in-out;
			-o-transition: all 0.5s ease-in-out;
			transition: all 0.5s ease-in-out;
		}
		.customowlprev{
			position: absolute;
			bottom: 38px;
			background-color: #FFF;
			width: 80px;
		    background-image: url(images/arrow_left.png);
 			background-repeat: no-repeat;
            background-position: center;
			height: 40px;
		}
		.customowlprev:hover{
			 background-image: url(images/arrow_leftt-hover.png);
			-webkit-transition: all 0.5s ease-in-out;
			-moz-transition: all 0.5s ease-in-out;
			-o-transition: all 0.5s ease-in-out;
			transition: all 0.5s ease-in-out;
		}
    </style>
    
<script>
$(window).on("load resize",function(){
    var offsetctn = $('.container').offset();
    var innomechw = $('.innomech').width() + 40;
    $('.bg_innomech > span, .bg_news > span').css('width', offsetctn.left + innomechw );
});
</script>
    
</body>

</html>