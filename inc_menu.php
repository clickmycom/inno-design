<link type="text/css" rel="stylesheet" href="css/input-radio-style.css">
<script src="js/modal-steps.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('input:not(:checked)').parent().find(".contentpay").slideUp();
		$('input:checked').parent().find(".contentpay").slideDown();
		$('input').click(function () {
			$('input:not(:checked)').parent().find(".contentpay").slideUp();
			$('input:checked').parent().find(".contentpay").slideDown();
		});
		
		$(".modal-signin-up-next").click(function(){
			var step = $(this).attr("step");
			if(!isNaN(step)){
				if(step == 2 || step == 3){
					if($(window).width() >= 900){
						$('#myModal').find(".modal-dialog").css("width","900");
					}
				}else{
					$('#myModal').find(".modal-dialog").css("width","");
				}
				$(".modal-signin-up").addClass("hide");
				$(".modal-signin-up[step='" + step + "']").removeClass("hide");
			}else{
				$('#myModal').modal("toggle");
				setTimeout(function(){
					$(".modal-signin-up").addClass("hide");
					$(".modal-signin-up[step='1']").removeClass("hide");
				},2000);
			}
			
		});
		$('#myModal').css("height",$(window).height());
	});

	$(window).resize(function(){
		$('#myModal').css("height",$(window).height());
	});
</script>
<style>
	.mainnavbar {
		z-index: 6899;
		position: relative;
	}
	
	.top_bar {
		height: 45px;
		background-color: #144e8e;
		text-align: right;
	}
	
	.logo {
		text-align: left;
	}
	
	.logo a {
		display: inline-block;
		position: relative;
		margin-top: -10px;
	}
	
	.logo a img {
		max-width: 100%;
		max-height: 100px;
		height: auto;
		display: block;
	}
	
	.mainmenu {
		text-align: right;
		padding-right: 0;
		position: static;
	}
	
	.mainmenu ul {
		display: block;
		padding: 0;
		margin-bottom: 0;
		margin-top: 35px;
	}
	
	.mainmenu ul li {
		display: inline-block;
		margin-left: 32px;
		padding-bottom: 32px;
	}
	
	.fz_l .mainmenu ul li {
		margin-left: 18px;
	}
	
	.mainmenu ul li a {
		font-size: 1rem;
		display: block;
		color: #000;
		text-decoration: none;
		-webkit-transition: all 0.5s ease-in-out;
		-moz-transition: all 0.5s ease-in-out;
		-o-transition: all 0.5s ease-in-out;
		transition: all 0.5s ease-in-out;
	}
	
	.mainmenu ul li a:hover {
		color: #144e8e;
	}
	
	.btn_menu {
		display: none;
	}
	
	.submenu {
		position: absolute;
		left: 0;
		width: 100%;
		top: 100%;
		border-top: 4px solid #144e8e;
		border-bottom: 4px solid #144e8e;
		background-color: #FFF;
		text-align: left;
		color: #000;
	}
	
	.submenu_left {
		border-right: 1px solid #9d9b9b;
		margin-top: 40px;
		margin-bottom: 15px;
	}
	
	.submenu_left h1 {
		font-size: 2.5rem;
		line-height: 0.7;
		margin-bottom: 0;
	}
	
	.submenu_left h2 {
		font-weight: 300;
		font-size: 2rem;
		line-height: 0.7;
		margin-top: 0;
	}
	
	.submenu_left p {
		color: #aeacac;
		font-size: 0.8rem;
		line-height: 0.9;
	}
	
	.mainmenu ul li .submenu_left a {
		display: inline-block;
		font-size: 0.8rem;
		border: 1px solid #9b9b9b;
		color: #858484;
		padding: 5px 20px 3px 20px;
		margin-top: 15px;
	}
	
	.mainmenu ul li .submenu_left a:hover {
		color: #144e8e;
		border: 1px solid #144e8e;
	}
	
	.submenu_right img {
		width: 100%;
		height: auto;
		display: block;
	}
	
	.submenu_mid ul.submenu_mid_list {
		columns: 2;
		-webkit-columns: 2;
		-moz-columns: 2;
	}
	
	.submenu_mid ul.submenu_mid_list > li {
		padding: 0;
		background-image: url(images/icon_submenu.jpg);
		background-repeat: no-repeat;
		background-position: left 9px;
		padding-left: 17px;
		display: block;
		-webkit-column-break-inside: avoid;
		page-break-inside: avoid;
		break-inside: avoid-column;
	}
	
	.submenu_mid ul.submenu_mid_list > li > a {
		color: #144e8e;
	}
	
	.submenu_mid {
		margin-bottom: 15px;
	}
	
	.submenu_mid ul li a {
		font-weight: 500;
		color: #000;
		text-decoration: none;
	}
	
	.submenu_mid ul li ul {
		margin: 0;
		list-style: disc;
		padding-left: 0px;
	}
	
	.submenu_mid ul li ul li {
		color: #818181;
		display: list-item;
		padding: 0;
	}
	
	.submenu_mid ul li ul li a {
		font-size: 0.9rem;
		font-weight: normal;
		color: #000;
	}
	
	.submenu_mid ul li a:hover {
		color: #144e8e;
	}
	
	.fontsize_btn {
		display: inline-block;
		vertical-align: middle;
		margin-top: 5px;
		padding-top: 5px;
		position: relative;
		padding-left: 7px;
		margin-left: 12px;
	}
	
	.fontsize_btn::before {
		content: "";
		border-left: 1px solid #fff;
		position: absolute;
		left: 0;
		opacity: 0.4;
		height: 15px;
		top: 8px;
	}
	
	.fontsize_btn div {
		display: inline-block;
		color: #FFF;
		font-weight: 500;
		cursor: pointer;
		line-height: 26px;
		padding: 0 2px;
		margin-left: 1px;
	}
	
	.fontsize_s {
		font-size: 18px;
	}
	
	.fontsize_m {
		font-size: 24px;
	}
	
	.fontsize_l {
		font-size: 35px;
	}
	
	.flag_btn {
		display: inline-block;
		margin-left: 10px;
		vertical-align: top;
		margin-top: 5px;
	}
	
	.flag_btn a {
		display: inline-block;
		margin-left: 2px;
		vertical-align: middle;
	}
	
	.icon_top {
		display: inline-block;
		padding-left: 7px;
		margin-left: 12px;
		position: relative;
		vertical-align: top;
		padding-bottom: 8px;
		padding-top: 8px;
		position: relative;
	}
	
	.icon_top::before {
		content: "";
		border-left: 1px solid #fff;
		position: absolute;
		left: 0;
		opacity: 0.4;
		height: 15px;
		top: 15px;
	}
	
	.icon_top a {
		display: inline-block;
		margin-left: 7px;
		vertical-align: top;
	}
	
	.searchbox {
		display: inline-block;
		width: 0;
		overflow: hidden;
		-webkit-transition: all 0.5s ease-in-out;
		-moz-transition: all 0.5s ease-in-out;
		-o-transition: all 0.5s ease-in-out;
		transition: all 0.5s ease-in-out;
	}
	
	.searchbox input {
		font-size: 20px;
		width: 100%;
		height: 28px;
		display: block;
		padding-left: 5px;
		padding-right: 5px;
		background-color: transparent;
		border: 0;
		color: #FFF;
		border-bottom: 1px solid #fff;
	}
	
	.searchbox input:focus {
		outline: 0;
	}
	
	.searchbox.active {
		width: 275px;
	}
	
	.searchbox .search_btn {
		display: none;
	}
	
	.searchbox input::-webkit-input-placeholder {
		/* Chrome/Opera/Safari */
		color: #b3bfd3;
	}
	
	.searchbox input::-moz-placeholder {
		/* Firefox 19+ */
		color: #b3bfd3;
	}
	
	.searchbox input:-ms-input-placeholder {
		/* IE 10+ */
		color: #b3bfd3;
	}
	
	.searchbox input:-moz-placeholder {
		/* Firefox 18- */
		color: #b3bfd3;
	}
	
	.social_btn{
        position: relative;
        display: inline-block;
        vertical-align: top;
        margin-left: 5px;
        cursor: pointer;
    }
    .social_btn i{
        color: #fff;
    }
    .social{
        position: absolute;
        top: 100%;
        right: 0;
        background-color: #144e8e;
        padding: 10px 10px 5px 5px;
        text-align: left;
        display: none;
    }
    .social a{
        white-space: nowrap;
        color: #FFF;
        line-height: 1;
        margin-top: 5px;
        font-size: 0.8rem;
    }
    .social a:hover{
        text-decoration: none;
    }
    .social img{
        display: inline-block;
        width: 18px;
        margin-right: 5px;
    }
    
    .icon_top i{
        color: #fff;
        font-size: 1.1rem;
        vertical-align: top;
        line-height: 30px;
    }
    .homeicon i{
        font-size: 1.2rem;
    }
    .btn-dropdown i{
        font-size: 1.1rem;
    }
    .social i{
        width: 20px;
        text-align: center;
        font-size: 16px;
        vertical-align: top;
        margin-right: 5px;
        line-height: 16px;
    }
    
	@media (min-width: 992px) {
		.submenu {
			z-index: -9;
			opacity: 0;
			visibility: hidden;
			-webkit-transition: all 0.5s ease-in-out;
			-moz-transition: all 0.5s ease-in-out;
			-o-transition: all 0.5s ease-in-out;
			transition: all 0.5s ease-in-out;
		}
		.hassub:hover .submenu {
			opacity: 1.0;
			z-index: 99998;
			visibility: visible;
		}
	}
	
	@media (max-width: 1199px) {
		.mainmenu {
			padding-right: 15px;
		}
		.mainmenu ul li {
			margin-left: 12px;
		}
	}
	
	@media (max-width: 991px) {
		.btn_menu {
			display: inline-block;
			margin-top: 20px;
		}
		.btn_menu .btn_menu_line {
			width: 25px;
			display: inline-block;
			padding-right: 5px;
			padding-top: 1px;
			vertical-align: middle;
		}
		.btn_menu .btn_menu_line span {
			display: block;
			height: 3px;
			background-color: #3a3a3a;
			margin-bottom: 3px;
		}
		.btn_menu .btn_menu_text {
			display: inline-block;
			font-size: 1.2rem;
			font-weight: 500;
			letter-spacing: 1px;
			vertical-align: middle;
		}
		.logo a img {
			max-height: 70px;
		}
		.mainmenu > ul {
			position: absolute;
			left: 0;
			background-color: #FFF;
			margin-top: 0;
			top: 100%;
			width: 100%;
			padding-top: 15px;
			padding-bottom: 15px;
			border-top: 2px solid #144e8e;
			border-bottom: 2px solid #144e8e;
			display: none;
		}
		.mainmenu > ul > li {
			padding-bottom: 0;
			display: block;
			margin: 0;
			text-align: left;
		}
		.mainmenu > ul > li > a {
			font-size: 1.2rem;
			padding-left: 15px;
			padding-right: 15px;
		}
		.submenu {
			position: static;
			display: none;
		}
		.submenu_left h1 {
			font-size: 1.3rem;
		}
		.submenu_left h2 {
			font-size: 1.1rem;
		}
		.submenu_mid ul.submenu_mid_list > li {
			margin-left: 0;
		}
		.submenu_left {
			margin-top: 0;
		}
		.searchbox.active {
		width: 205px;
	}
	
	}
	
	@media (max-width: 767px) {
		.icon_top a{
			margin-left: 0;
		}
		.submenu_mid ul.submenu_mid_list {
			margin-top: 0;
		}
		.submenu_right img {
			max-width: 400px;
		}
		.member-signin,
		.fontsize_btn,
		.flag_btn{
			display: inline-block;
			vertical-align: top;
		}
		
		.fontsize_btn {
			padding-top: 1px;
			margin-left: 5px;
			margin-top: 0px;
			
		}
		.submenu_mid ul.submenu_mid_list {
			columns: 1;
			-webkit-columns: 1;
			-moz-columns: 1;
		}
		.submenu_right {
			display: none;
		}
		.top_bar {
			height: auto;
			-webkit-transition: all 0.5s ease-in-out;
			-moz-transition: all 0.5s ease-in-out;
			-o-transition: all 0.5s ease-in-out;
			transition: all 0.5s ease-in-out;
		}
		.searchbox {
			position: absolute;
			bottom: -20px;
			right: 15px;
			opacity: 0;
			visibility: hidden;
		}
		.searchbox.active {
			opacity: 1.0;
			visibility: visible;
			width: 90vw;
		}
		.top_bar.active {
			padding-bottom: 30px;
		}
		.searchbox input {
			width: 0;
			display: inline-block;
		}
		.searchbox.active input {
			width: 80%;
		}
		.searchbox .search_btn {
			display: inline-block;
		}
		.top_bar.active .icon_top > a.search_btn {
			background-image: url(images/icon_close.png);
			background-position: center;
			-webkit-transition: all 0.5s ease-in-out;
			-moz-transition: all 0.5s ease-in-out;
			-o-transition: all 0.5s ease-in-out;
			transition: all 0.5s ease-in-out;
		}
		.top_bar.active .icon_top > a.search_btn img {
			opacity: 0;
		}
		.submenu_left {
			display: none;
		}
		.submenu_mid {
			padding-top: 10px;
		}
	}
</style>
<nav class="row mainnavbar wow fadeInDown">
	<div class="col-xs-12 top_bar">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="member-signin">
							<div class="circle-ppl"><img src="images/profile_03.png"></div> <a href="profile.php">Sirawat Phakamas</a> <a href="#" class="status-class">SMEs</a> </div>
						<div class="fontsize_btn">
							<div class="fontsize_s">A</div>
							<div class="fontsize_m">A</div>
							<div class="fontsize_l">A</div>
						</div>
						<div class="flag_btn">
							<a href="#"><img src="images/th.jpg"></a>
							<a href="#"><img src="images/en.jpg"></a>
<!--							<a href="#"><img src="images/jp.jpg"></a>-->
						</div>
						<div class="icon_top">
							<a href="index.php" class="homeicon"><!--<img src="images/icon_home.png">--><i class="material-icons">home</i></a>
							<div class="dropdown dropdown-hover open-sigin-up-button">
								<label for="dropdown-2" class="btn-dropdown"><!--<img src="images/login.png">--><i class="material-icons">lock</i></label>
								<!--<input class="dropdown-open" type="checkbox" id="dropdown-2" name="dropdown-2" aria-hidden="true" hidden />
								<label for="dropdown-2" class="dropdown-overlay"></label>-->
								<div class="dropdown-inner">
									<input id="textinput" name="textinput" type="text" placeholder="Username :" class="form-control">
									<input id="textinput" name="textinput" type="text" placeholder="Password :" class="form-control">
									<a href="#" class="signin-up-button">
										<button class="graybtn">LOGIN</button>
									</a> 
									<div class="row">
										<div class="col-xs-6 col-sm-6">
										<a href="#" class="login-button">
										<button class="nostyle btn_registr" data-toggle="modal" data-target="#myModal">Register</button>
									</a> 
										</div>
										<div class="col-xs-6 col-sm-6">
										<a href="#" class="forget-pass">Forget Password</a> </div>
										</div>
									</div>
									
									
							</div>
							<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/close_03.png"></span></button>
										</div>
										<div class="modal-body">
											<div class="row modal-signin-up" step="1">
												<div class="jumbotron">Register</div>
												<div class="content-popup">
													<div class="row">
														<div class="col-xs-6 col-sm-6">
															<a href="#"><img src="images/register_03.png"> </a>
															<div>Connect with Facebook</div>
														</div>
														<div class="col-xs-6 col-sm-6">
															<a href="#" class="modal-signin-up-next" step="2"><img src="images/register_05.png"> </a>
															<div>Register</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row modal-signin-up hide" step="2">
												<div class="jumbotron">Register</div>
												<div class="content-popup bg-gray2 regis-form">
													<div class="row">
														<div class="col-sm-6">
															<label>ชื่อล็อกอิน (ต้องการ)</label>
															<input id="textinput" name="textinput" type="text" placeholder="Username" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>อีเมล(ต้องการ)</label>
															<input id="textinput" name="textinput" type="text" placeholder="Email" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>รหัสผ่าน</label>
															<input id="textinput" name="textinput" type="text" placeholder="Password must longer than 8" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>ยืนยันรหัสผ่าน(ต้องการ)</label>
															<input id="textinput" name="textinput" type="text" placeholder="Re-Enter Password" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>หมายเลขบัตรประชาชน (ต้องการ)</label>
															<input id="textinput" name="textinput" type="text" placeholder="ID cart" class="form-regis"> </div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<label>คำนำหน้า</label>
															<select id="selectbasic" name="selectbasic" class="form-regis">
																<option value="1">คำนำหน้า</option>
																<option value="2">Option two</option>
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<label>ชื่อ (ต้องการ)</label>
															<input id="textinput" name="textinput" type="text" placeholder="ชื่อ" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>นามสกุล (ต้องการ)</label>
															<input id="textinput" name="textinput" type="text" placeholder="นามสกุล" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>ชื่อ (EN)</label>
															<input id="textinput" name="textinput" type="text" placeholder="Name" class="form-regis"> </div>
														<div class="col-sm-6">
															<label>นามสกุล (EN)</label>
															<input id="textinput" name="textinput" type="text" placeholder="Surname" class="form-regis"> </div>
													</div>
													<div class="row mt-5">
														<div class="col-sm-6"> <img src="images/capcha_07.png" class="img-responsive"> </div>
													</div>
												</div>
												<div class="btn-sub text-center">
													<a href="#" class="modal-signin-up-next" step="3">
														<button class="btn-sending">NEXT</button>
													</a>
												</div>
											</div>
											<div class="row modal-signin-up hide" step="3">
												<div class="jumbotron"> CREATE YOUR PROFILE</div>
												<div class="content-popup regis-form">
												<div class="bg-gray2">
													<div class="row">
														<div class="col-sm-12 head_link_2">
															<h1>Who are you?</h1> </div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-1" type="checkbox" />
																<label for="section-1">
																	<p>คุณมีแหล่งเงินทุนของตัวเอง หรือไม่ก็ เป็นการกู้เงินจากสถาบันการเงินเป็นหลัก</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-2" type="checkbox" />
																<label for="section-2">
																	<p>คุณมีรูปแบบการผลิตและบริการ ไม่เกิน 50 ล้านบาท</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-3" type="checkbox" />
																<label for="section-3">
																	<p>คุณมีจำนวนการจ้างงาน ไม่เกิน 50 คน</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-4" type="checkbox" />
																<label for="section-4">
																	<p>คุณมีรูปแบบการผลิตและบริการ มากกว่า 50 ล้านบาท</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-5" type="checkbox" />
																<label for="section-5">
																	<p>คุณมีจำนวนการจ้างงานมากกว่า 50 คน</p>
																</label>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-6" type="checkbox" />
																<label for="section-6">
																	<p>คุณมีธุรกิจที่มีการเติบโตอย่างรวดเร็ว แบบก้าวกระโดด</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-7" type="checkbox" />
																<label for="section-7">
																	<p>คุณมีเงินทุนจากการระดมทุน จากนักลงทุน ทั่วโลกโดยการขายไอเดียของธุรกิจ</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-8" type="checkbox" />
																<label for="section-8">
																	<p>คุณทำงานอยู่ในหน่วยงานของรัฐ</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-9" type="checkbox" />
																<label for="section-9">
																	<p>คุณกำลังศึกษาอยู่ในระดับอุดมศึกษา / มหาวิทยาลัย</p>
																</label>
															</div>
														</div>
													</div>
													</div>
													<div class="bg-gray2">
													<div class="row">
														<div class="col-sm-12 head_link_2">
															<h1>What is your interest ?</h1> </div>
													</div>
													<div class="row">
														<div class="col-sm-4">
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-10" type="checkbox" />
																<label for="section-10">
																	<p>Product Innovation</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-11" type="checkbox" />
																<label for="section-11">
																	<p>Services Innovation</p>
																</label>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-12" type="checkbox" />
																<label for="section-12">
																	<p>Process Innovation</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-13" type="checkbox" />
																<label for="section-13">
																	<p>Business Innovation</p>
																</label>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-14" type="checkbox" />
																<label for="section-14">
																	<p>Research Innovation</p>
																</label>
															</div>
															<div class="md-radio md-radio-inline radiocheck">
																<input id="section-15" type="checkbox" />
																<label for="section-15">
																	<p>Resource Innovation</p>
																</label>
															</div>
														</div>
													</div>
													</div>
												</div>
												<div class="btn-sub text-center">
													<a href="#" class="modal-signin-up-next" step="4">
														<button class="btn-sending">Submit</button>
													</a>
												</div>
										</div>
											<div class="row modal-signin-up hide" step="4">
												<div class="jumbotron"> Register</div>
												
												<div class="confirm-page">
													<img src="images/confirm-pic_07.png">
													<div>คุณคือ  SMEs</div>
													</div>
												
												<div class="btn-sub text-center">
													<a href="#" class="modal-signin-up-next">
														<button class="btn-sending2">ตกลง</button>
													</a>
												</div>
										</div>
										</div>
									</div>
								</div>
							</div>
							<div class="social_btn" href="#"><!--<img src="images/share.png">--><i class="material-icons">share</i>
                                <div class="social">
                                    <a href="#"><i class="fab fa-facebook-f"></i> Facebook</a>
                                    <a href="#"><i class="fab fa-youtube"></i> Youtube</a>
                                    <a href="#"><i class="fab fa-instagram"></i> Instragram</a>
                                    <a href="#"><i class="fab fa-twitter"></i> Twitter</a>
                                    <a href="#"><i class="fab fa-line"></i> Line@</a>
                                </div>
                            </div>
							<div class="searchbox">
								<input type="text" placeholder="search">
								<a class="search_btn" href="#"><img src="images/icon_search.png"></a>
							</div>
							<a class="search_btn" href="#"><!--<img src="images/icon_search.png">--><i class="material-icons">search</i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3 logo">
				<a href="index.php"><img src="images/nia_logo.png"></a>
			</div>
			<div class="col-xs-6 col-sm-9 mainmenu">
				<div class="btn_menu">
					<div class="btn_menu_line"><span></span><span></span><span></span></div>
					<div class="btn_menu_text">MENU</div>
				</div>
				<ul>
					<li class="hassub"><a href="#">เกี่ยวกับเรา</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#" target="_parent">องค์กร</a>
												<ul>
													<li><a href="http://nia.clickmycom.com/article-view/1" target="_parent">ความเป็นมา</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/23" target="_parent">วิสัยทัศน์</a></li>
												</ul>
											</li>
											<li><a href="http://nia.clickmycom.com/article-view/24" target="_parent">นโยบาย</a>
												<ul>
													<li><a href="http://nia.clickmycom.com/article-view/24" target="_parent">นโยบายผู้บริหาร</a></li>
													<li><a href="#" target="_parent">ยุทธศาสตร์</a>
														<ul>
															<li><a href="http://nia.clickmycom.com/article-view/17" target="_parent">Area-based Innovation</a></li>
															<li><a href="#" target="_parent">Value Chain Innovation</a>
																<ul>
																	<li><a href="http://nia.clickmycom.com/article-view/27" target="_parent">Innovation for Economy</a></li>
																	<li><a href="http://nia.clickmycom.com/article-view/28" target="_parent">Innovation for Society</a></li>
																</ul>
															</li>
															<li><a href="http://nia.clickmycom.com/article-view/30" target="_parent">Innovation Network</a></li>
															<li><a href="http://nia.clickmycom.com/article-view/31" target="_parent">Market Innovation</a></li>
															<li><a href="http://nia.clickmycom.com/article-view/33" target="_parent">Innovation Informatics</a></li>
														</ul>
													</li>
													<li><a href="http://nia.clickmycom.com/article-view/34" target="_parent">แผนดำเนินงาน</a></li>
												</ul>
											</li>
											<li><a href="http://nia.clickmycom.com/article-view/2" target="_parent">อุทยานนวัตกรรม</a>
												<ul>
													<li><a href="http://nia.clickmycom.com/article-view/19" target="_parent">อุทยานนวัตกรรม</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/21" target="_parent">1: OPEN INNOVATION SPACE</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/22" target="_parent">M: IDEATION SPACE</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/35" target="_parent">2: IOT CITY INNOVATION CENTER</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/36" target="_parent">3: CO-INNOVATING SPACE</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/37" target="_parent">4: INNOVATION STRATEGY SPACE</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/38" target="_parent">5: INNOTORIUM SPACE</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/39" target="_parent">6: IMPLEMENTORIUM SPACE</a></li>
												</ul>
											</li>
											<li><a href="#" target="_parent">การดำเนินงาน</a>
												<ul>
													<li><a href="http://nia.clickmycom.com/article-view/40" target="_parent">ผลการดำเนินงาน</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/41" target="_parent">กฎหมาย ระเบียบ ข้อบังคับ</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/42" target="_parent">การกำกับดูแลกิจการที่ดี</a></li>
												</ul>
											</li>
											<li><a href="http://nia.clickmycom.com/article-view/43" target="_parent">ศูนย์ข้อมูลข่าวสารอิเล็กทรอนิกส์</a></li>
											<li><a href="http://nia.clickmycom.com/article-view/44" target="_parent">ดาวน์โหลด</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
					<li class="hassub"><a href="#">ยุทธศาสาตร์นวัตกรรม</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#" target="_parent">สื่อนวัตกรรม</a>
												<ul>
													<li><a href="http://nia.clickmycom.com/article-view/48" target="_parent">วิดีทัศน์</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/12" target="_parent">บล็อก</a></li>
													<li><a href="http://nia.clickmycom.com/article-view/50" target="_parent">อินโฟกราฟฟิค</a>
														<ul>
															<li><a href="#" target="_parent">ยุทธศาสตร์</a></li>
															<li><a href="#" target="_parent">กลไกสนับสนุน</a></li>
														</ul>
													</li>
													<li><a href="http://nia.clickmycom.com/article-view/52" target="_parent">หนังสือ</a></li>
												</ul>
											</li>
											<li><a href="http://nia.clickmycom.com/article-view/11" target="_parent">เปิดโลกนวัตกรรม</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
					<li class="hassub"><a href="#">อุทยานนวัตกรรม</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#">Area-baseb Innovation</a></li>
											<li><a href="#">Value Chain Innovation</a>
												<ul>
													<li><a href="#">Innovation for Economy</a></li>
													<li><a href="#">Innovation for Society</a></li>
												</ul>
											</li>
											<li><a href="#">Innovation Capability</a></li>
											<li><a href="#">Innovation Network</a></li>
											<li><a href="#">Market Innovation</a></li>
											<li><a href="#">Innovation Informatics</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
					<li class="hassub"><a href="#">เปิดโลกนวัตกรรม</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#">Area-baseb Innovation</a></li>
											<li><a href="#">Value Chain Innovation</a>
												<ul>
													<li><a href="#">Innovation for Economy</a></li>
													<li><a href="#">Innovation for Society</a></li>
												</ul>
											</li>
											<li><a href="#">Innovation Capability</a></li>
											<li><a href="#">Innovation Network</a></li>
											<li><a href="#">Market Innovation</a></li>
											<li><a href="#">Innovation Informatics</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
					<li class="hassub"><a href="#">สื่อนวัตกรรม</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#">Area-baseb Innovation</a></li>
											<li><a href="#">Value Chain Innovation</a>
												<ul>
													<li><a href="#">Innovation for Economy</a></li>
													<li><a href="#">Innovation for Society</a></li>
												</ul>
											</li>
											<li><a href="#">Innovation Capability</a></li>
											<li><a href="#">Innovation Network</a></li>
											<li><a href="#">Market Innovation</a></li>
											<li><a href="#">Innovation Informatics</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
					<li class="hassub"><a href="#">บุคลากร</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#">Area-baseb Innovation</a></li>
											<li><a href="#">Value Chain Innovation</a>
												<ul>
													<li><a href="#">Innovation for Economy</a></li>
													<li><a href="#">Innovation for Society</a></li>
												</ul>
											</li>
											<li><a href="#">Innovation Capability</a></li>
											<li><a href="#">Innovation Network</a></li>
											<li><a href="#">Market Innovation</a></li>
											<li><a href="#">Innovation Informatics</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
					<li class="hassub"><a href="#">คำถามที่พบบ่อย</a>
						<div class="submenu">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-3 submenu_left">
										<hgroup>
											<h1>INNOVATION</h1>
											<h2>STRATEGIC</h2> </hgroup>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> <a href="#">รายละเอียด</a> </div>
									<div class="col-xs-12 col-sm-6 submenu_mid">
										<ul class="submenu_mid_list">
											<li><a href="#">Area-baseb Innovation</a></li>
											<li><a href="#">Value Chain Innovation</a>
												<ul>
													<li><a href="#">Innovation for Economy</a></li>
													<li><a href="#">Innovation for Society</a></li>
												</ul>
											</li>
											<li><a href="#">Innovation Capability</a></li>
											<li><a href="#">Innovation Network</a></li>
											<li><a href="#">Market Innovation</a></li>
											<li><a href="#">Innovation Informatics</a></li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 submenu_right"> <img src="images/submenu_nia.jpg"> </div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<script>
	$('#myModal').modalSteps({
		btnCancelHtml: "Cancel"
		, btnPreviousHtml: "Previous"
		, btnNextHtml: "Next"
		, btnLastStepHtml: "Complete"
		, disableNextButton: false
		, completeCallback: function () {}
		, callbacks: function() {}
		, getTitleAndStep: function () {}
	});

	$('.login-button').on('click', function (event) {
		$(this).parent().parent().parent().addClass("hide");
	});
	
	$('.open-sigin-up-button').on('click', function (event) {
		
        if (  $(this).children( ".dropdown-inner" ).css('opacity') == 0) {
            $(this).children( ".dropdown-inner" ).css({'opacity': 1, 'visibility': 'visible'});
	  } else {
          $(this).children( ".dropdown-inner" ).css({'opacity': 0, 'visibility': 'hidden'});
	  }
        event.preventDefault();
		//if(event.target.className != "nostyle"){
		//	$('.login-button').parent().parent().parent().removeClass("hide");
		//}
	});

	$('.open-sigin-up-button').on('mouseover', function (event) {
		$('.login-button').parent().parent().parent().removeClass("hide");
	});

</script>
<script>
	$(document).ready(function () {
		$('.btn_menu').click(function (event) {
			if ($(".mainmenu > ul").is(":hidden")) {
				$(this).addClass("active");
				$('.mainmenu > ul').slideDown();
			}
			else {
				$('.mainmenu > ul').slideUp();
				$(this).removeClass("active");
				$('.submenu').slideUp();
				$('.hassub').removeClass("active");
			}
			event.stopPropagation();
		});
		$('.hassub').click(function (event) {
			if ($(this).children(".submenu").is(":hidden")) {
				$('.hassub').removeClass("active");
				$(this).addClass("active");
				$('.submenu').slideUp();
				$(this).children(".submenu").slideDown();
			}
			else {
				if (Modernizr.mq('(max-width: 991px)')) {
					$('.submenu').slideUp();
					$(this).removeClass("active");
				}
			}
			event.stopPropagation();
		});
		$('.search_btn').click(function (event) {
			$('.searchbox').toggleClass('active');
			$('.top_bar').toggleClass('active');
			event.stopPropagation();
		});
		$('.social_btn').click(function (event) {
			$('.social').slideToggle();
			event.stopPropagation();
		});
		$('.searchbox').click(function (event) {
			event.stopPropagation();
		});
		$('html').click(function (event) {
			$('.searchbox').removeClass('active');
			$('.top_bar').removeClass('active');
			$('.social').slideUp();
		});
		$('.fontsize_s').click(function (event) {
			$('html').removeClass('fz_l');
			$('html').addClass('fz_s');
		});
		$('.fontsize_m').click(function (event) {
			$('html').removeClass('fz_l fz_s');
		});
		$('.fontsize_l').click(function (event) {
			$('html').removeClass('fz_s');
			$('html').addClass('fz_l');
		});
		$( '.dropdown-inner' ).click(function (event) {
			event.stopPropagation();
		});
	});
</script>