<!doctype html>
<html>

<head>
    <?php require('inc_header.php'); ?>
        <link type="text/css" rel="stylesheet" href="css/input-radio-style.css">
        <script type="text/javascript">
            $(document).ready(function () {
                $('input:not(:checked)').parent().find(".contentpay").slideUp();
                $('input:checked').parent().find(".contentpay").slideDown();
                $('input').click(function () {
                    $('input:not(:checked)').parent().find(".contentpay").slideUp();
                    $('input:checked').parent().find(".contentpay").slideDown();
                });
            });
        </script>
</head>

<body>
    <style>
        .mainnavbar {
            border-bottom: 1px solid #e1e1e1;
        }
        
        .head_link {
            margin-left: 15px;
        }
        
        .head_link h1 {
            font-weight: 500;
        }

        @media (max-width: 767px) {
            .head_link{
                margin-left:0px;
            }
        }

    </style>
    <div class="container-fluid">
        <?php require('inc_menu.php'); ?>
                <div class="container">
                    <section class="row wow fadeInDown">
                        <div class="col-sm-3 profile-img"> <img src="images/profile_03.png" class="img-responsive"> </div>
                        <div class="col-sm-6 profile">
                            <h1>My Profile</h1> Name : Sirawat Phakamas
                            <br> E-mail : Sirawat@gmail.com
                            <br> Who are you : SMEs </div>
                       
                    </section>
                    <section class="row wow fadeInDown">
                        <div class="col-xs-12 head_link">
                            <h1>Who are you?</h1> </div>
                    </section>
                    <section class="row wow fadeInDown">
                        <div class="col-sm-12">
                            <div class="bg-gray">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-16" type="checkbox" checked="checked" />
                                            <label for="section-16">
                                                <p>คุณมีแหล่งเงินทุนของตัวเอง หรือไม่ก็ เป็นการกู้เงินจากสถาบันการเงินเป็นหลัก</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-17" type="checkbox" />
                                            <label for="section-17">
                                                <p>คุณมีรูปแบบการผลิตและบริการ ไม่เกิน 50 ล้านบาท</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-18" type="checkbox" />
                                            <label for="section-18">
                                                <p>คุณมีจำนวนการจ้างงาน ไม่เกิน 50 คน</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-19" type="checkbox" checked="checked"/>
                                            <label for="section-19">
                                                <p>คุณมีรูปแบบการผลิตและบริการ มากกว่า 50 ล้านบาท</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-20" type="checkbox" checked="checked" />
                                            <label for="section-20">
                                                <p>คุณมีจำนวนการจ้างงานมากกว่า 50 คน</p>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-21" type="checkbox" />
                                            <label for="section-21">
                                                <p>คุณมีธุรกิจที่มีการเติบโตอย่างรวดเร็ว แบบก้าวกระโดด</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-22" type="checkbox" />
                                            <label for="section-22">
                                                <p>คุณมีเงินทุนจากการระดมทุน จากนักลงทุน ทั่วโลกโดยการขายไอเดียของธุรกิจ</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-23" type="checkbox" />
                                            <label for="section-23">
                                                <p>คุณทำงานอยู่ในหน่วยงานของรัฐ</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-24" type="checkbox" />
                                            <label for="section-24">
                                                <p>คุณกำลังศึกษาอยู่ในระดับอุดมศึกษา / มหาวิทยาลัย</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-sub">
                                <a href="#">
                                    <button class="btn-sending">SAVE</button>
                                </a>
                            </div>
                        </div>
                    </section>
                    <section class="row wow fadeInDown">
                        <div class="col-xs-12 head_link">
                            <h1>What is your interest ?</h1> </div>
                    </section>
                    <section class="row wow fadeInDown">
                        <div class="col-sm-12">
                            <div class="bg-gray">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-25" type="checkbox" />
                                            <label for="section-25">
                                                <p>Product Innovation</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-26" type="checkbox" />
                                            <label for="section-26">
                                                <p>Services Innovation</p>
                                            </label>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-27" type="checkbox" />
                                            <label for="section-27">
                                                <p>Process Innovation</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-28" type="checkbox" />
                                            <label for="section-28">
                                                <p>Business Innovation</p>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-29" type="checkbox" />
                                            <label for="section-29">
                                                <p>Research Innovation</p>
                                            </label>
                                        </div>
                                        <div class="md-radio md-radio-inline radiocheck">
                                            <input id="section-30" type="checkbox" />
                                            <label for="section-30">
                                                <p>Resource Innovation</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-sub pad-mo">
                                <a href="#">
                                    <button class="btn-sending">SAVE</button>
                                </a>
                            </div>
                        </div>
                    </section>
                </div>
            <?php require('inc_footer.php'); ?>
    </div>
</body>

</html>