<style>
    .line_footer_left{
        height: 21px;
        background-color: #850e19;
        margin-bottom: 40px;
    }
    .line_footer_right{
        height: 21px;
        background-color: #64050e;
        margin-bottom: 40px;
    }
    .bg_footer{
        background-color: #073262;
        padding-bottom: 40px;
    }
    .nav_footer ul{
        list-style: none;
        margin: 0;
        padding-left: 0;
    }
    .nav_footer li{
        display: block;
        line-height: 1;
    }
    .nav_footer li a{
        display: inline-block;
        color: #FFF;
        font-size: 0.9rem;
        opacity: 0.5;
        -webkit-transition: all  0.5s ease-in-out;
        -moz-transition:all  0.5s ease-in-out;
        -o-transition: all  0.5s ease-in-out;
        transition: all  0.5s ease-in-out;
        text-decoration: none;
    }
    .nav_footer li a:hover{
        opacity: 1.0;
    }
    .contact_footer{
        color: #FFF;
        padding-right: 45px;
    }
    .contact_footer address, .contact_footer div{
        opacity: 0.5;
        line-height: 1;
        margin-bottom: 0;
        font-size: 0.9rem;
        -webkit-transition: all  0.5s ease-in-out;
        -moz-transition:all  0.5s ease-in-out;
        -o-transition: all  0.5s ease-in-out;
        transition: all  0.5s ease-in-out;
    }
    .contact_footer h4{
        opacity: 0.5;
        line-height: 1;
        margin-bottom: 2px;
        -webkit-transition: all  0.5s ease-in-out;
        -moz-transition:all  0.5s ease-in-out;
        -o-transition: all  0.5s ease-in-out;
        transition: all  0.5s ease-in-out;
    }
    .contact_footer h4:hover, .contact_footer address:hover, .contact_footer div:hover{
        opacity: 1.0;
    }
    .copyright{
        color: #FFF;
        opacity: 0.5;
        -webkit-transition: all  0.5s ease-in-out;
        -moz-transition:all  0.5s ease-in-out;
        -o-transition: all  0.5s ease-in-out;
        transition: all  0.5s ease-in-out;
        font-size: 0.9rem;
    }
    .copyright:hover{
        opacity: 1;
    }
    .link_social a{
        display: inline-block;
        margin-right: 7px;
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .contact_footer h4.fth402{
        margin-top: 25px;
    }
    .link_social a .fab{
        color: #FFF;
        width: 32px;
        height: 32px;
        line-height: 28px;
        font-size: 14px;
        border: 2px solid #FFF;
        border-radius: 50%;
        text-align: center;
        vertical-align: bottom;
    }
    .link_social a .fab.fa-instagram{
        font-size: 16px;
    }
    .link_social a .fab.fa-line{
        font-size: 26px;
        overflow: hidden;
        color: #073262;
        background-color: #073262;
        position: relative;
    }
    .link_social a .fab.fa-line::before{
        position: relative;
        z-index: 3;
    }
    .link_social a .fab.fa-line::after{
        content: "";
        background-color: #FFF;
        width: 18px;
        height: 18px;
        position: absolute;
        left: 6px;
        top: 6px;;
        z-index: 1;
    }
@media (max-width: 767px){
    .contact_footer{
        margin-top: 10px;
    }
    .copyright{
        margin-top: 10px;
    }
    .link_social{
        margin-top: 10px;
    }
    .bg_footer{
        padding-bottom: 20px;
    }
    .line_footer_left, .line_footer_right{
        margin-bottom: 15px;
        height: 11px;
    }
    .contact_footer h4{
        font-size: 1.2rem;
    }
    .nav_footer{
        display: none;
    }
}
</style>

<footer class="row bg_footer wow fadeInDown">
    <div class="col-xs-9 line_footer_left"></div><div class="col-xs-3 line_footer_right"></div>
    <div class="container">
        <div class="row">
            <nav class="col-xs-12 col-sm-4 nav_footer">
                <ul>
                    <li><a href="#">หน้าหลัก</a></li>
                    <li><a href="#">เกี่ยวกับเรา</a></li>
                    <li><a href="#">ยุทธศาสตร์นวัตกรรม</a></li>
                    <li><a href="#">อุทยานนวัตกรรม</a></li>
                    <li><a href="#">เปิดโลกนวัตกรรม</a></li>
                    <li><a href="#">ขอรับการสนับสนุนโครงการนวัตกรรม</a></li>
                    <li><a href="#">จัดซื้อจัดจ้าง</a></li>
                    <li><a href="#">ติดต่อเรา</a></li>
                </ul>
            </nav>
            <div class="col-xs-12 col-sm-4 contact_footer">
                <h4>สำนักงานนวัตกรรมแห่งชาติ (สนช.)</h4>
                <div>องค์กรหลักในการเสริมสร้างระบบนวัตกรรมแห่งชาติเพื่อเพิ่มคุณค่าที่ยั่งยืน</div>
                <h4 class="fth402">นวัตกรรม</h4>
                <div>
                    การใช้ความรู้และความคิดสร้างสรรค์นำไปสู่การเปลี่ยนแปลงเพื่อก่อให้เกิดคุณค่า (innovation: making Creativity into Value Reality)
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 contact_footer">
                <h4>สำนักงานนวัตกรรมแห่งชาติ (องค์การมหาชน)</h4>
                <address>ที่อยู่: 73/2 ถนนพระรามที่ 6 แขวงทุ่งพญาไท เขตราชเทวี
                กรุงเทพฯ 10400 </address>
                <div>โทรศัพท์: 02-017 5555 ,โทรสาร: 02-017 5566</div>
                <div>อีเมล์: info@nia.or.th</div>
                <div class="link_social">
                    <a href="#"><i class="fab fa-facebook-f"></i></a><a href="#"><i class="fab fa-youtube"></i></a><a href="#"><i class="fab fa-instagram"></i></a><a href="#"><i class="fab fa-twitter"></i></a><a href="#"><span><i class="fab fa-line"></i></span></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-sm-offset-8 copyright">© Copyright 2018. All Rights Reserved.</div>
        </div>
    </div>
</footer>